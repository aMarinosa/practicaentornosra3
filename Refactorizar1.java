package Refactorizaci�n;


import java.util.Scanner; // Estas importando todos los paquetes de java cuando solo necesitas una clase de un paquete (En este caso el Scanner).
/**
 * 
 * @author aMarinosa
 * @since 31-01-2018
 */
public class Refactorizar1 {	// El nombre de la clase siempre tiene que empezar por mayuscula.
	// final static String cad = "Bienvenido al programa"; No hay ninguna necesidad de que sea static siendo que solo se usa en la clase main. De hecho, no hay necesidad de crear la variable siendo que lo que quieres hacer puedes ponerlo en un syso y as� ahorras codigo.
	final static String SALUDO = "Bienvenido al programa"; // Como es una constante, se pone en mayusculas.
	public static void main(String[] args) {
		
		int numeroa = 7;	
		int numerob = 8;
		int numeroc = 25; 
		// La variable c es la que usa el scanner, lo que yo har�a
		//para que el codigo fuese mas limpio ser�a llamar a esta variable c y al scanner sc.
		//No le tiene coherencia a llamar a las dos primeras variables 'a,b' y a esta "numeroc".
		Scanner sc = new Scanner(System.in);
		
		System.out.println(SALUDO);
		System.out.println("Introduce tu dni");
		String dni = sc.nextLine(); //Ponerle un nombre que se asocie a lo que estas haciendo.
		System.out.println("Introduce tu nombre");
		String nombre = sc.nextLine();
		
		// Las variables se inicializan cuando las creas.
		// int numeroc = 25;
		//Todas las variables inicializadas tienen que estar al principio de la clase.
		
		if(numeroa > numerob || numeroc % 5 != 0 && ((numeroc * 3) - 1)> numerob / numeroc) // El codigo estaba muy junto y era ilegible.
																		// De esta forma es mucho m�s comprensible.
		{
			System.out.println("Se cumple la condici�n");
		}
		
		numeroc = numeroa + numerob * numeroc + numerob / numeroa; // Separar 
		
		String[] diasSemana = //new String[7]; //Los corchetes tienen que estar al lado del tipo de variable, porque es lo que indica que es un vector, si esta al lado .
			{
					"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"
			}; // Poner los valores del vector de esta forma es mas limpio.
		
		showWeekDays(diasSemana);
	}
	
	static void showWeekDays(String[] weekDays) //Los corchetes tienen que estar al lado del tipo de variable,
												//porque es lo que indica que es un vector, si esta al lado .
	{
		for(int dia=0;dia<7;dia++) 
		{
			System.out.println("El dia de la semana en el que te encuentras ["+(dia+1)+"-7] es el dia: "+ weekDays[dia]);
		}
	}
	
}