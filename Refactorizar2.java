package Refactorizaci�n;
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.util.Scanner;
/**
 * 
 * @author aMarinosa
 * @since 20-02-2018
 */
public class Refactorizar2 {

	static Scanner sc = new Scanner(System.in); // Todo en la misma linea.
	public static void main(String[] args) 
	{

		int cantidad_maxima_alumnos = 10; // Darle el valor en la misma linea que es creada.
		int[] notas = new int[10]; // Corchetes al lado del tipo de variable.
		for(int n = 0; n < 10; n++) // Separar codigo para que sea m�s entendible.
		{
			System.out.println("Introduce nota media de alumno");
			notas[n] = sc.nextInt();
		}	
		
		System.out.println("El resultado es: " + recorrer_array(notas));
		
		sc.close();
	}
	static double recorrer_array(int[] vector) // Corchetes al lado del tipo de variable.
	{
		double acumulador = 0; 
		//Cambiar el nombre de la variable con una que resuma m�s su funci�n.
		for(int i = 0; i < 10; i++) //Hacer c�digo m�s limpio.
		{
			acumulador += vector[i]; //Ahorrar codigo poniendo += es m�s eficiente.
		}
		return acumulador/10;
	}
	
}